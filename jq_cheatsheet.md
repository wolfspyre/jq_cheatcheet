> jq 'map(.foo)'

```
[ { foo: 1 }, { foo: 2 } ]
[1, 2]
```

> jq '.[] | .foo'

```
[ { foo: 1 }, { foo: 2 } ]
1, 2
```

> jq 'map({ a, b })'

``` javascript
[ { a: 1, b: 2, c: 3 }, ...]
[ { a: 1, b: 2 }, ...]
```

> jq 'with_entries(.value |= fromjson)' --sort-keys

```
{ b: "{}", a: "{}" }
{ a: {}, b: {} }
```

> jq 'with_entries(.value |= tojson)' --sort-keys

```
{ a: {}, b: {} }
{ b: "{}", a: "{}" }
```

> jq 'flatten(1)'

```
[[1], [2]]
[1, 2]
```
